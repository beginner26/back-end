package Day03;

/* Assignment 1
 *  Latihan menggunakan array
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Panjang Array Integer = ");
        int panjangArray = input.nextInt();
        int[] inputArray = new int[panjangArray];

        // Mengisi element array
        for (int i = 0; i < inputArray.length; i++) {
            System.out.print("Index[" + i + "] = ");
            inputArray[i] = input.nextInt();
        }

        // Mencetak array yang sudah di input ke console
        System.out.println("Panjang Array Integer adalah " + panjangArray);
        System.out.print("Array integer [");
        for (int angka : inputArray) {
            System.out.print(" " + angka + " ");
        }
        System.out.print("]");

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}