package Day03;

/* Assignment 5_b
 *  Latihan menggunakan Map-HashMap
 */

import java.util.*;

public class Assignment5_b {
    public static void main(String[] args) {
        Scanner inputInt = new Scanner(System.in);
        Scanner inputStr = new Scanner(System.in);

        Map map = new HashMap();

        System.out.println("Membuat map dengan nilai <Key, value>");

        // Looping untuk mengisi data HashMap
        for (int i = 0; i < 3; i++) {
            int userInputInt = inputInt.nextInt();
            String userInputStr = inputStr.nextLine();
            map.put(userInputInt, userInputStr);
        }

        Set set = map.entrySet();// Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {
            // Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("Menambahkan satu data");
        int userInputInt = inputInt.nextInt();
        String userInputStr = inputStr.nextLine();
        map.put(userInputInt, userInputStr);

        set = map.entrySet();// Converting to Set so that we can traverse
        itr = set.iterator();
        while (itr.hasNext()) {
            // Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println("Menghapus satu data");
        userInputInt = inputInt.nextInt();
        map.remove(userInputInt);

        set = map.entrySet();// Converting to Set so that we can traverse
        itr = set.iterator();
        while (itr.hasNext()) {
            // Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        inputInt.close();
        inputStr.close();
    }
}
