package Day03;

/* Assignment 2
 *  Latihan menggunakan array 2 dimensi
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Menerima inputan untuk menentukan panjang array
        System.out.print("Baris Array Integer = ");
        int baris = input.nextInt();
        System.out.print("Kolom Array Integer = ");
        int kolom = input.nextInt();

        // Mendeklarasikan array 2 dimensi sesuai dengan inputan user
        int[][] arrayDuaDimensi = new int[baris][kolom];

        // Menginput element pada array
        for (int i = 0; i < arrayDuaDimensi.length; i++) {
            for (int j = 0; j < arrayDuaDimensi[i].length; j++) {
                System.out.print("Index[" + i + "," + j + "] = ");
                arrayDuaDimensi[i][j] = input.nextInt();
            }
        }

        // Menampilkan array ke console berdasarkan inputan dari user
        System.out.println("Baris dan Kolom Array 2 Dimensi adalah [" + baris + "," + kolom + "]");
        for (int[] arrayDimensi : arrayDuaDimensi) {
            for (int angka : arrayDimensi) {
                System.out.print(angka + "\t");
            }
            System.out.println(" ");
        }

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}