package Day03;
/* Assignment 5
 *  Latihan menggunakan ArrayList dan menggunakan built in method
 */

import java.util.*;

public class Assignment5 {

    // membuat method untuk menampilkan arraylist untuk menghindari repeat code
    public static void tampilArrayList(ArrayList al) {
        Iterator<String> itr = al.iterator();
        // Menampilkan nilai pada array list
        while (itr.hasNext()) {
            System.out.print(itr.next() + " ");
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // Mendeklarasikan array list
        ArrayList<String> al = new ArrayList<>();
        ArrayList<String> al2 = new ArrayList<>();

        String userInput;
        // Mengisi nilai pada array list
        System.out.println("Membuat array list dengan nilai");
        for (int i = 0; i < 3; i++) {
            userInput = input.nextLine();
            al.add(userInput);
        }

        tampilArrayList(al);

        // Menambahkan satu nilai pada array list
        System.out.println("\nMenambah satu data nama");
        userInput = input.nextLine();
        al.add(userInput);

        // Me reassign itr dengan nilai array list terbaru dan menampilkan nilai pada array list
        tampilArrayList(al);

        // Menghapus arralist1 berdasarkan arraylist2
        System.out.println("\n Menghapus satu data nama");
        userInput = input.nextLine();
        al2.add(userInput);
        al.removeAll(al2);

        tampilArrayList(al2);

        System.out.println("\n Meretain dua data nama");
        userInput = input.nextLine();
        al2.add(userInput);
        userInput = input.nextLine();
        al2.add(userInput);
        al.retainAll(al2);

        tampilArrayList(al);
        input.close();
    }
}