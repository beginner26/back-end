package Day03;

/* Assignment 3
 *  Latihan menggunakan algoritma bubble sort dan binary search
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int userInput, jumlahArray;
        int[] angkaArray = new int[10];
        do {
            System.out.println("---Masukan Menu---");
            System.out.println("1. Input Data Array");
            System.out.println("2. Melakukan Bubble Sort pada Array");
            System.out.println("3. Pencarian Elemen pada Array");
            System.out.println("4. Exit");
            System.out.print("Pilih menu = ");

            userInput = input.nextInt();

            switch (userInput) {
                // User menginput array
                case 1:
                    System.out.print("Masukan jumlah array = ");
                    jumlahArray = input.nextInt();
                    angkaArray = new int[jumlahArray];

                    for (int i = 0; i < jumlahArray; i++) {
                        System.out.print("Masukan angka ke " + (i + 1) + " = ");
                        angkaArray[i] = input.nextInt();
                    }
                    System.out.print("Array yang di input [ ");
                    for (int angka : angkaArray)
                        System.out.print(angka + " ");
                    System.out.println("] \n");
                    break;

                // Melakukan sorting memakai bubble sort
                case 2:
                    for (int i = 0; i < angkaArray.length - 1; i++) {
                        for (int j = 0; j < angkaArray.length - 1 - i; j++) {
                            if (angkaArray[j + 1] < angkaArray[j]) {
                                int temp = angkaArray[j];
                                angkaArray[j] = angkaArray[j + 1];
                                angkaArray[j + 1] = temp;
                            }
                        }
                    }
                    System.out.println("Berhasil melakukan sorting");
                    System.out.print("[ ");
                    for (int angka : angkaArray)
                        System.out.print(angka + " ");
                    System.out.println("] \n");
                    break;

                // Mencari angka menggunakan algoritma binary search
                case 3:
                    System.out.print("Masukan angka yang ingin dicari = ");
                    int target = input.nextInt();
                    int left = 0;
                    int middle;
                    int right = angkaArray.length - 1;
                    boolean found = false;

                    // Apabila target tidak ada di rentang array maka tidak perlu melakukan
                    // pencarian
                    if (target < angkaArray[0] || target > angkaArray[angkaArray.length - 1]) {
                        System.out.println("Angka yang di cari tidak ada di array \n");
                        break;
                    }

                    // Melakukan pencarian menggunakan binary search
                    while (left <= right) {
                        middle = (left + right) / 2;
                        if (angkaArray[middle] == target) {
                            System.out.println("Angka ketemu di index ke " + middle);
                            found = true;
                            System.out.println("\n");
                            break;
                        } else if (angkaArray[middle] < target) {
                            left = middle + 1;
                        } else if (angkaArray[middle] > target) {
                            right = middle - 1;
                        }
                    }
                    if (!found) {
                        System.out.println("Angka yang di cari tidak ada di array \n");
                    }
                    break;

                case 4:
                    System.out.println("---Keluar dari Program---");
                    break;

                default:
                    System.out.println("Harap pilih menu yang sesuai");
                    break;
            }

        } while (userInput != 4);

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}