package Day03;

/* Assignment 4
 *  Latihan menggunakan ArrayList dan Membuat Object
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.*;

public class Assignment4 {
    public static void main(String[] args) {

        // Membuat Class Mobil
        class Mobil {
            String merek;
            String warna;
            int tahunPembuatan;

            Mobil(String merek, String warna, int tahunPembuatan) {
                this.merek = merek;
                this.warna = warna;
                this.tahunPembuatan = tahunPembuatan;
            }
        }

        // Mendeklarasikan Array List
        ArrayList<Mobil> mbl = new ArrayList<Mobil>();

        Scanner inputInteger = new Scanner(System.in);
        Scanner inputString = new Scanner(System.in);
        int userInput;

        do {
            System.out.println("---Masukan Menu---");
            System.out.println("1. Input Object");
            System.out.println("2. Tampilkan Object");
            System.out.println("3. Exit");

            System.out.print("Pilih Menu = ");
            userInput = inputInteger.nextInt();

            switch (userInput) {
                // Membuat instansi baru berdasarakan inputan user
                case 1:
                    String merek, warna;
                    int tahunPembuatan;
                    System.out.print("Masukan Merek = ");
                    merek = inputString.nextLine();
                    System.out.print("Masukan Warna = ");
                    warna = inputString.nextLine();
                    System.out.print("Masukan Tahun = ");
                    tahunPembuatan = inputInteger.nextInt();

                    Mobil mobilBaru = new Mobil(merek, warna, tahunPembuatan);
                    mbl.add(mobilBaru);
                    System.out.println(" ");
                    break;

                // Mencetak object yang sudah diinput
                case 2:
                    Iterator itr = mbl.iterator();
                    while (itr.hasNext()) {
                        Mobil mb = (Mobil) itr.next();
                        System.out.println(" ");
                        System.out.println("Merek : " + mb.merek);
                        System.out.println("Warna : " + mb.warna);
                        System.out.println("Tahun Pembuatan : " + mb.tahunPembuatan);
                        System.out.println(" ");
                    }
                    break;

                case 3:
                    System.out.println("---Keluar Program---");
                    break;

                default:
                    System.out.println("Pilih menu yang ada");
                    break;
            }

        } while (userInput != 3);

        inputInteger.close();
        inputString.close();
    }
}