package Day01;

/* Assignment 1
 *  Belajar Concatenated Strings
 */
class Assignment1 {
    public static void main(String[] args) {
        // Mendeklarasikan variabel dengan bermancam tipe data
        String nama = "Naufal";
        String jenisKelamin = "Laki-laki";
        int umur = 24;
        float nilaiRerata = 8.5f;

        // Syntax berikut cara concate string variabel
        System.out.println("Seorang siswa " + jenisKelamin + " bernama " + nama + ", berumur " + umur
                + " Tahun, memiliki nilai rata-rata " + nilaiRerata);
    }
}