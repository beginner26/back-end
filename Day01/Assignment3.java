package Day01;

/* Assignment 3
 *  Menggunakan operator pada Java
 */
class Assignment3 {
    public static void main(String[] args) {
        // Melakukan perhitungan luas persegi
        int panjang = 12;
        int lebar = 8;
        int luasPersegi = panjang * lebar;
        System.out.println("---Soal Point 1---");
        System.out.println("Luas Persegi " + luasPersegi);

        // Melakukan perhitungan luas segitiga
        float alas = 7.0f;
        float tinggi = 3.5f;
        double luasSegitiga = 0.5 * alas * tinggi;
        System.out.println("---Soal Point 2---");
        System.out.println("Luas Segitiga " + luasSegitiga);

        // Melakukan komparasi terhadap luasPersegi dengan luasSegitiga
        boolean hasilPerhitungan = luasPersegi > luasSegitiga;
        System.out.println("---Soal Point 3---");
        System.out.println("Hasil perhitungan point1 > point2 adalah " + hasilPerhitungan);

        // Melakukan decrement dan increment pada variabel luasPersegi
        System.out.println("---Soal Point 4---");
        System.out.println("Decrement 3x");
        System.out.println(--luasPersegi);
        System.out.println(--luasPersegi);
        System.out.println(--luasPersegi);
        System.out.println("Increment 6x");
        System.out.println(++luasPersegi);
        System.out.println(++luasPersegi);
        System.out.println(++luasPersegi);
        System.out.println(++luasPersegi);
        System.out.println(++luasPersegi);
        System.out.println(++luasPersegi);
    }
}