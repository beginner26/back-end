package Day01;
/* Assignment 4
 *  Mendapatkan input dari user menggunakan Scanner, dan melakukan perhitungan
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

class Assignment4 {
    public static void main(String[] args) {
        // Membuat instance input dari Object Scanner
        Scanner input = new Scanner(System.in);

        // Melakukan perhitungan volume balok berdasarkan input dari user
        System.out.println("--- Perhitungan Volume Balok ---");
        System.out.print("Masukan Panjang: ");
        int panjang = input.nextInt();
        System.out.print("Masukan Lebar: ");
        int lebar = input.nextInt();
        System.out.print("Masukan Tinggi: ");
        int tinggi = input.nextInt();
        // Rumus menghitung volume balok
        int volumeBalok = panjang * lebar * tinggi;
        System.out.println("Volume Balok adalah " + volumeBalok);

        // Melakukan perhitungan volume bola berdasarkan input dari user
        System.out.println("--- Perhitungan Volume Bola ---");
        System.out.print("Masukan Pi: ");
        float Pi = input.nextFloat();
        System.out.print("Masukan Jari-jari: ");
        int r = input.nextInt();
        // Rumus menghitung volume bola
        double volumeBola = (4.0 / 3) * Pi * r * r * r;
        System.out.printf("Volume Bola adalah %.2f \n", volumeBola);
        input.close();
    }
}