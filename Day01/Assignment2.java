package Day01;

/* Assignment 2
 * Belajar mengguanakan escape sequence
 */
class Assignment2 {
    public static void main(String[] args) {
        // Mendeklarasikan variabel dengan bermancam tipe data
        boolean booleanType = true;
        byte byteType = 120;
        short shortType = 240;
        int integerType = 2000;
        long longType = 10000;
        double doubleType = 64.64;
        float floatType = 16.16f;
        char charType = 'z';
        String stringType = "G2Academy";

        // Berikut adalah cara penggunaan escape sequences, dan print formating
        System.out.printf(
                "Boolean \n%b \nByte \n%d \nShort \n%d \nInteger \n%d \nLong \n%d \nDouble \n%f \nFloat \n%f \nChar \n%s \nString \n%s",
                booleanType, byteType, shortType, integerType, longType, doubleType, floatType, charType, stringType);
    }
}