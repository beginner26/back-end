package Day01;

// Ini adalah program java pertama saya
class HelloWorld {
    // Main Method adalah method yang pertama kali di eksekusi saat program berjalan
    public static void main(String[] args) {
        // Syntax untuk menampilkan ke console
        System.out.println("Hello, World");
    }
}