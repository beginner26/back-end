package comp.Assignment1.socket;

import comp.Assignment1.properties.CrunchifyGetPropertyValues;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class MyClient {
    public static void main(String[] args) {
        Socket socket = null;
        DataInputStream din = null;
        DataOutputStream dout = null;
        Scanner input = new Scanner(System.in);

        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
        try {
            // Mengakses config.properties
            int port = Integer.parseInt(config.getPropValues("PORT"));
            String ip = config.getPropValues("IP");

            // Membuat sebuah stream socket dan menghubungkan ke port & ip yang telah di spesifikasikan
            socket = new Socket(ip, port);
            din = new DataInputStream(socket.getInputStream());

            // Me return Outputsream yang terpasang dengan socket
            OutputStream outputStream = socket.getOutputStream();
            dout = new DataOutputStream(outputStream);

            String textDariServer = "";
            String textKeClient = "";

            while (!textDariServer.equalsIgnoreCase("exit")) {
                textDariServer = input.nextLine();

                dout.writeUTF(textDariServer);
                dout.flush();

                textKeClient = din.readUTF();
                System.out.println("Pesan dari Server: " + textKeClient);

            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                input.close();
                din.close();
                dout.close();
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}