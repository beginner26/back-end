package comp.Assignment2.socket;

import comp.Assignment2.properties.CrunchifyGetPropertyValues;
import comp.Assignment2.Validasi;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class MyServer {
    public static void main(String[] args) {
        Validasi val = new Validasi();
        Scanner input = new Scanner(System.in);
        CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();

        System.out.println("Login");
        System.out.print("Masukan email: ");
        String email = input.next();
        System.out.print("Masukan password: ");
        String password = input.next();

//        if(val.validateEmail(email) && val.validatePassword(password)) {
        if (true) {
            System.out.println("Berhasil Login");
            System.out.println("Menunggu request dari client...");

            DataOutputStream dout = null;
            ServerSocket ss = null;
            Socket s = null;
            DataInputStream dis = null;
            try {
                int port = Integer.parseInt(config.getPropValues("PORT"));
//                System.out.println(port);

                ss = new ServerSocket(port);
                s = ss.accept();//establishes connection

                dis = new DataInputStream(s.getInputStream());

                String str = "";
                while (!str.equalsIgnoreCase("exit")) {
                    str = (String) dis.readUTF();
                    if (str.equalsIgnoreCase("exit")) break;
                    String[] parseData = str.split("\n");

                    for (String data : parseData) {
                        String[] oneData = data.split(",");
                        System.out.println("Nama: " + oneData[0]);
                        System.out.println("Nilai Fisika: " + oneData[1]);
                        System.out.println("Nilai Biologi: " + oneData[2]);
                        System.out.println("Nilai Kimia: " + oneData[3]);
                    }

                    dout = new DataOutputStream(s.getOutputStream());
                    dout.writeUTF("Server response: Data done processing");
                    dout.flush();
                }
                ss.close();

            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

}
