package comp.Assignment2.socket;


import comp.Assignment2.properties.CrunchifyGetPropertyValues;
import comp.Assignment2.Validasi;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class MyClient {
    public static void main(String[] args) {
        Validasi val = new Validasi();
        Scanner input = new Scanner(System.in);

        System.out.println("Login");
        System.out.print("Masukan email: ");
        String email = input.next();
        System.out.print("Masukan password: ");
        String password = input.next();

//        if(val.validateEmail(email) && val.validatePassword(password)) {
        if (true) {
            int userPilih = 0;
            String data = "";
            CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
            int port = 0;
            String ip = "";
            String namaFile = "";

            Socket s = new Socket();
            DataOutputStream dout = null;
            DataInputStream dis = null;

            while (userPilih != 4) {
                System.out.println("----- Menu -----");
                System.out.println("1. Connect Socket");
                System.out.println("2. Send Data to Server");
                System.out.println("3. Close Socket");
                System.out.println("4. Exit");

                System.out.print("Pilih Menu: ");
                userPilih = input.nextInt();

                switch (userPilih) {
                    case 1:
                        try {
                            port = Integer.parseInt(config.getPropValues("PORT"));
                            ip = config.getPropValues("IP");

                            s = new Socket(ip, port);
                            System.out.println("Berhasil Connect ke Socket");
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;

                    case 2:
                        try {
                            // Load file reader dan masukan data ke string data
                            System.out.print("Masukan nama file: ");
                            namaFile = input.next();
                            FileReader fr = new FileReader("C:\\tmp\\" + namaFile + ".txt");
                            BufferedReader br = new BufferedReader(fr);
                            int i;
                            while ((i = br.read()) != -1) {
                                data = data + (char) i;
                            }
                            System.out.println("Success load data");
                            br.close();
                            fr.close();

                            dout = new DataOutputStream(s.getOutputStream());
                            dout.writeUTF(data);
                            dout.flush();

                            dis = new DataInputStream(s.getInputStream());
                            System.out.println(dis.readUTF());
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;

                    case 3:
                        try {
                            s.close();
                            System.out.println("Socket Closed");
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;
                    case 4:
                        try {
                            dout = new DataOutputStream(s.getOutputStream());
                            dout.writeUTF("exit");
                            dout.flush();
                            System.out.println("Keluar dari Program");
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;
                }
            }
        }
    }
}