package comp.Assignment3;

import java.util.regex.Pattern;

public class Validasi {
    public boolean validateEmail(String email) {
        // Email must follow this format [huruf&angka]@[provide].[domain]
        return Pattern.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$", email);
    }

    public boolean validatePassword(String password) {
        // Password must contain 8 characters, at least one uppercase, one lowercase, one spescial character and one number
        return Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", password);
    }
}