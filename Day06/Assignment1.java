package Day06;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class Assignment1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);

        int userInput = 0;
        String direktori, namaFile, isiFile;
        while (userInput != 3) {
            System.out.println("1. Tulis String");
            System.out.println("2. Baca String");
            System.out.print("Pilih: ");
            userInput = input.nextInt();
            switch (userInput) {
                case 1:
                    // Menerima input dari user, untuk penempatan file
                    System.out.print("Input Direktory: ");
                    direktori = inputS.nextLine();
                    System.out.print("Input Nama File: ");
                    namaFile = inputS.nextLine();
                    System.out.print("Input Isi File: ");
                    isiFile = inputS.nextLine();
                    try {
                        // Membuat File Output Stream
                        FileOutputStream fout = new FileOutputStream(direktori + namaFile);
                        byte b[] = isiFile.getBytes();//converting string into byte array
                        fout.write(b);
                        fout.close();
                        System.out.println("success...");
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;

                case 2:
                    // Memerima inputan user, dan mencetak file ke console
                    System.out.print("Input Direktory: ");
                    direktori = inputS.nextLine();
                    System.out.print("Input Nama File: ");
                    namaFile = inputS.nextLine();
                    try {
                        FileInputStream fin = new FileInputStream(direktori + namaFile);
                        int i = 0;
                        while ((i = fin.read()) != -1) {
                            System.out.print((char) i);
                        }
                        System.out.println("");
                        fin.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
            }
        }
    }
}
