package Day06;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Assignment2_b {
    public static void main(String[] args) {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);


        ArrayList<StaffIO> arrStaff = new ArrayList<>();

        int userInput = 0;

        String direktori, namaFile;
        String dataString = "";

        while (userInput != 3) {
            System.out.println("Menu");
            System.out.println("1. Load Data Staff");
            System.out.println("2. Tampilkan Data All Staff ke Layar Console");
            System.out.println("3. EXIT");

            System.out.print("Pilih menu: ");
            userInput = inputI.nextInt();

            switch (userInput) {
                case 1:
                    try {
                        // Menerima inputan
                        System.out.print("Masukan Direktori: ");
                        direktori = inputS.nextLine();
                        System.out.print("Masukan Nama File: ");
                        namaFile = inputS.nextLine();
                        FileReader fr = new FileReader(direktori + namaFile);
                        int i;

                        while ((i = fr.read()) != -1) {
                            dataString = dataString + (char) i;
                        }

                        String[] allStaff = dataString.split("\n");

                        for (int idx = 1; idx < allStaff.length; idx++) {
                            String[] data = allStaff[idx].split(",");
                            arrStaff.add(new StaffIO(Integer.parseInt(data[0]), data[1], Integer.parseInt(data[2])));
                        }

                        fr.close();
                        System.out.println("Berhasil meload data");

                    } catch (Exception e) {
                        System.out.println("Error");
                    }
                    break;

                case 2:
                    for (StaffIO dataStaffIO : arrStaff) {

                        System.out.println("ID : " + dataStaffIO.id);
                        System.out.println("Nama : " + dataStaffIO.nama);
                        System.out.println("Total Gaji : " + dataStaffIO.gajiPokok);
                        System.out.println("Terbilang : " + dataStaffIO.angkaToTerbilang((long) dataStaffIO.gajiPokok));
                    }
                    break;

                case 3:
                    System.out.println("Keluar Dari Program");
                    break;

                default:
                    System.out.println("Pilih sesuai menu");
                    break;
            }
        }
    }
}
