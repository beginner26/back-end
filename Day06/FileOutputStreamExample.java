package Day06;

import java.io.FileOutputStream;

public class FileOutputStreamExample {
    public static void main(String args[]) {

        try {
            FileOutputStream fout = new FileOutputStream("C:\\tmp\\test.txt");
            fout.write(78);
            fout.write(97);
            fout.write(117);
            fout.write(102);
            fout.write(97);
            fout.write(108);
            fout.close();
            System.out.println("success...");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
