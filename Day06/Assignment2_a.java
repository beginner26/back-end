package Day06;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Assignment2_a {
    public static void main(String[] args) {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);


        ArrayList<StaffIO> arrStaff = new ArrayList<>();

        // Membuat data dummy
        arrStaff.add(new StaffIO(1, "Mamat", 1000000));
        arrStaff.add(new StaffIO(3, "Joko", 3000000));
        arrStaff.add(new StaffIO(2, "Udin", 2000000));

        StaffIO newStaffIO = new StaffIO(-1, "", 0);

        int userInput = 0;
        int target = -1;

        String direktori, namaFile;

        while (userInput != 4) {
            System.out.println("Menu");
            System.out.println("1. Input Data Staff");
            System.out.println("2. Proses Data Staff");
            System.out.println("3. Tulis Data Staff ke File");
            System.out.println("4. EXIT");

            System.out.print("Pilih menu: ");
            userInput = inputI.nextInt();

            switch (userInput) {
                case 1:
                    // Cek id sebelum dimasukan ke array list
                    boolean bolehInput = true;

                    // 1. Masukan ID
                    System.out.print("Input ID: ");
                    int id = inputI.nextInt();
                    // 2. Cek apakah ID sudah ada
                    for (StaffIO dataStaff : arrStaff) {
                        if (dataStaff.id == id) {
                            bolehInput = false;
                        }
                    }
                    // 3. Jika ID sudah ada maka masukan ulang ID
                    while (!bolehInput) {
                        System.out.println("ID sudah ada, masukan ID yang lain");
                        System.out.print("Input ID: ");
                        id = inputI.nextInt();
                        bolehInput = true;
                        // 2. Cek lagi apakah ID sudah ada
                        for (StaffIO dataStaff : arrStaff) {
                            if (dataStaff.id == id) {
                                bolehInput = false;
                            }
                        }
                    }

                    // 4. Jika ID belum ada maka buat object baru
                    System.out.print("Input Nama: ");
                    String nama = inputS.nextLine();
                    System.out.print("Input Gapok: ");
                    int gajiPokok = inputI.nextInt();
                    StaffIO newStaff = new StaffIO(id, nama, gajiPokok);
                    arrStaff.add(newStaff);
                    break;

                case 2:
                    // Menghitung tunjangan dan total gaji
                    for (StaffIO dataStaff : arrStaff) {
                        dataStaff.hitungTunjanganMakan();
                        dataStaff.hitungTunjanganTransport();
                        dataStaff.hitungTotalGaji();
                    }
                    System.out.println("Berhasil menghitung tunjangan & total gaji");
                    break;

                case 3:
                    // Sorting arraylist berdasarkan ID pada object
                    Collections.sort(arrStaff, new Comparator<StaffIO>() {
                        @Override
                        public int compare(StaffIO o1, StaffIO o2) {
                            return o1.id - o2.id;
                        }
                    });

                    System.out.print("Masukan direktori = ");
                    direktori = inputS.nextLine();
                    System.out.print("Masukan nama file = ");
                    namaFile = inputS.nextLine();


                    // Membuat file dan mengisi data berdasarkan arraylist
                    try {
                        FileWriter fw = new FileWriter(direktori + namaFile);
                        fw.write("ID, Nama, Total Gaji \n");
                        for (StaffIO dataStaffIO : arrStaff) {
                            fw.write(dataStaffIO.id + "," + dataStaffIO.nama + "," + dataStaffIO.totalGaji + "\n");
                        }
                        fw.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    System.out.println("Success...");
                    break;

                case 4:
                    System.out.println("Keluar Dari Program");
                    break;

                default:
                    System.out.println("Pilih sesuai menu");
                    break;
            }
        }
    }
}
