package Week2Day04;

class Parent {
    String name = "Mr. USD";
    int money = 2000000;

    public void home() {
        System.out.println("Parent's Home");
    }

    public void car() {
        System.out.println("Parent's Car");
    }
}

class Child extends Parent {
    String name = "Tom";
    int money = 200;

    public void car() {
        System.out.println("Child's Car");
    }

    public void callBothInfo() {
        super.home();
        super.car();
        System.out.println("Child's Car");
    }

    public void showParentInfo() {
        System.out.println("Parent Name: " + super.name);
        System.out.println("Parent Money: " + super.money);
    }

    public void showChildInfo() {
        System.out.println("Child Name: " + this.name);
        System.out.println("Child Money: " + this.money);
    }
}

public class Assignment3 {
    public static void main(String[] args) {
        Child c = new Child();

        c.callBothInfo();
        System.out.println("");
        c.showParentInfo();
        System.out.println("");
        c.showChildInfo();
    }
}
