//package Week2Day04;
//
//class Parent {
//    public void home() {
//        System.out.println("Parent's Home");
//    }
//
//    public void car() {
//        System.out.println("Parent's Car");
//    }
//}
//
//class Child extends Parent {
//    public void car() {
//        System.out.println("Child's Car");
//    }
//
//    public void callCarParent() {
//        super.car();
//    }
//}
//
//public class Day02.Assignment2 {
//    public static void main(String[] args) {
//        Parent p = new Parent();
//        Child c = new Child();
//
//        p.home();
//        c.callCarParent();
//        c.car();
//    }
//}
