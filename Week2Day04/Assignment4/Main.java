package Week2Day04.Assignment4;

import java.util.*;

public class Main {
    Map m = new HashMap();
    Scanner input = new Scanner(System.in);

    void inputDummy() {
        m.put(3, new Staff(3, "Shawn", "Director"));
        m.put(1, new Staff(1, "Tomy", "Supervisor"));
        m.put(2, new Staff(2, "John", "Director"));
    }

    public void addStaff() {

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();
        System.out.print("Input Jabatan : ");
        String jabatan = this.input.next();

        Staff st = new Staff(id, nama, jabatan);

        m.put(id, st);

    }

    public void tambahAbsensi() {

        System.out.print("Masukin ID: ");
        int id = this.input.nextInt();

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if (st.getId() == id) {
                st.tambahAbsensi();
                System.out.println("Berhasil menambahkan absensi");
            }
        }
    }

    public void viewStaff() {
        TreeMap<Integer, Staff> tm = new TreeMap<Integer, Staff>(m);
        Iterator itr = tm.keySet().iterator();
        System.out.println("ID\tNama\tJabatan\t\tAbsensi/hari");
        while (itr.hasNext()) {
            int key = (int) itr.next();


//            System.out.printf("%s\t%s\t%s\t%s\n", st.getId(), st.getNama(), st.getJabatan(), st.getAbsensi());
        }

//        Set set = m.entrySet();//Converting to Set so that we can traverse
//        Iterator itr = set.iterator();
//        System.out.println("ID\tNama\tJabatan\tAbsensi/hari");
//        while (itr.hasNext()) {
//
//            //Converting to Map.Entry so that we can get key and value separately
//            Map.Entry entry = (Map.Entry) itr.next();
//            //Convert to Staff
//            Staff st = (Staff) entry.getValue();
//
//
//            System.out.printf("%s\t%s\t%s\t%s\n", st.getId(), st.getNama(), st.getJabatan(), st.absensi);
//        }

    }

    public static void main(String args[]) {

        Main obj = new Main();

        int pil = 0;

        do {
            System.out.println("MENU");
//            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Buat Staff");
            System.out.println("2. Tambah Absensi");
            System.out.println("3. Tampilkan Data Staff");

            System.out.println("4. EXIT");

            System.out.print("Input nomor : ");
            pil = obj.input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    obj.addStaff();
                    break;
                case 2:
                    obj.tambahAbsensi();
                    break;
                case 3:
                    obj.viewStaff();
                    break;

                case 0:
                    obj.inputDummy();
                    break;
            }
            System.out.println();

        } while (pil != 4);

        // closing the scanner object
        obj.input.close();

    }


}