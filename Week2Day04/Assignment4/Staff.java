package Week2Day04.Assignment4;

abstract class Worker {
    int id;
    String nama;
    int absensi;

    abstract void tambahAbsensi();
}

public class Staff extends Worker {
    String jabatan;

    Staff(int id, String nama, String jabatan) {
        this.id = id;
        this.nama = nama;
        this.jabatan = jabatan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    public void tambahAbsensi() {
        this.absensi++;
    }
}