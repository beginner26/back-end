package Day04;

/* Assignment 5
 *  Latihan menggunakan split method untuk parse sebuah string
 */

import Day04.model.Mahasiswa_tugas5;

import java.util.Scanner;

public class Assignment5 {
    public static void main(String[] args) {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);

        int userPilih = 0;
        Mahasiswa_tugas5 mhsObj = new Mahasiswa_tugas5("", 0, 0, 0);
        int kimia, fisika, biologi;
        String nama, dataMhs = "";

        while (userPilih != 3) {
            System.out.println("Menu");
            System.out.println("1. Input data mahasiswa (Create accumulate data)");
            System.out.println("2. Parsing and print data");
            System.out.println("3. EXIT");

            System.out.print("Pilih Menu = ");
            userPilih = inputI.nextInt();


            switch (userPilih) {
                case 1:
                    System.out.println("Input data mahasiswa (Create accumulate data) ");
                    System.out.print("Masukan Nama = ");
                    nama = inputS.nextLine();
                    System.out.print("Masukan Nilai Fisika = ");
                    fisika = inputI.nextInt();
                    System.out.print("Masukan Nilai Biologi = ");
                    biologi = inputI.nextInt();
                    System.out.print("Masukan Nilai Kimia = ");
                    kimia = inputI.nextInt();
                    mhsObj = new Mahasiswa_tugas5(nama, fisika, biologi, kimia);
                    dataMhs = mhsObj.getData();
                    System.out.println(dataMhs);
                    break;

                case 2:
                    System.out.println("Parsing and print data ");
                    String dataString = mhsObj.getData();
//                    System.out.println(dataString);
                    String[] arrMhs = dataString.split(",");
                    for (int i = 0; i < arrMhs.length; i++) {
                        if (i == 0) {
                            System.out.println(arrMhs[i]);
                        } else {
                            System.out.print(arrMhs[i] + " ");
                        }
                    }
                    System.out.println("");
                    break;

                default:
                    System.out.println("Masukan pilihan yang sesuai");
                    break;
            }
        }
    }
}
