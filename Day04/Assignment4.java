package Day04;
/* Assignment 4
 *  Latihan menggunakan Java Constructor
 */

import Day04.model.Mahasiswa;

import java.util.Scanner;

public class Assignment4 {
    public static void main(String[] args) {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);

        int userPilih = 0;

        while (userPilih != 4) {
            System.out.println("Menu");
            System.out.println("1. Konstructor tanpa parameter");
            System.out.println("2. Konstructor dengan 2 parameter");
            System.out.println("3. Konstructor dengan 5 parameter");
            System.out.println("4. EXIT");

            System.out.print("Pilih Menu = ");
            userPilih = inputI.nextInt();

            int kimia, fisika, biologi;
            String nama, jurusan;

            switch (userPilih) {
                case 1:
                    Mahasiswa mhs = new Mahasiswa();
                    break;
                case 2:
                    System.out.print("Masukan Nama = ");
                    nama = inputS.nextLine();
                    System.out.print("Masukan Jurusan = ");
                    jurusan = inputS.nextLine();

                    Mahasiswa mhs2 = new Mahasiswa(nama, jurusan);
                    break;
                case 3:
                    System.out.print("Masukan Nama = ");
                    nama = inputS.nextLine();
                    System.out.print("Masukan Jurusan = ");
                    jurusan = inputS.nextLine();
                    System.out.print("Masukan Nilai Fisika = ");
                    fisika = inputI.nextInt();
                    System.out.print("Masukan Nilai Biologi = ");
                    biologi = inputI.nextInt();
                    System.out.print("Masukan Nilai Kimia = ");
                    kimia = inputI.nextInt();
                    Mahasiswa mhs5 = new Mahasiswa(nama, jurusan, fisika, biologi, kimia);
                    break;

                case 4:
                    System.out.println("Keluar Program");
                    break;

                default:
                    System.out.println("Masukan pilihan yang sesuai");
                    break;
            }
        }
    }
}
