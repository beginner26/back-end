package Day04;
/* Assignment 2
 *  Latihan menggunakan Java Methods
 */

import java.util.Scanner;

public class Assignment2 {
    public static int penambahan(int num1, int num2) {
        System.out.print("Penambahan " + num1 + "+" + num2 + " = ");
        return num1 + num2;
    }

    public static int pengurangan(int num1, int num2) {
        System.out.print("Pengurangan " + num1 + "-" + num2 + " = ");
        return num1 - num2;
    }

    public static int perkalian(int num1, int num2) {
        System.out.print("Perkalian " + num1 + "*" + num2 + " = ");
        return num1 * num2;
    }

    public static int pembagian(int num1, int num2) {
        System.out.print("Perkalian " + num1 + "*" + num2 + " = ");
        return num1 / num2;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int userPilih = 0;
        while (userPilih != 5) {
            System.out.println("Kalkulator");
            System.out.println("1. Penambahan");
            System.out.println("2. Pengurangan");
            System.out.println("3. Perkalian");
            System.out.println("4. Pembagian");
            System.out.println("5. Exit");

            System.out.print("Pilih Menu = ");
            userPilih = input.nextInt();

            int userInput1, userInput2;
            System.out.print("Masukan Nomor 1 = ");
            userInput1 = input.nextInt();
            System.out.print("Masukan Nomor 2 = ");
            userInput2 = input.nextInt();

            switch (userPilih) {
                case 1:
                    System.out.println(penambahan(userInput1, userInput2));
                    break;
                case 2:
                    System.out.println(pengurangan(userInput1, userInput2));
                    break;
                case 3:
                    System.out.println(perkalian(userInput1, userInput2));
                    break;
                case 4:
                    System.out.println(pembagian(userInput1, userInput2));
                    break;
                case 5:
                    System.out.println("Keluar Program");
                    break;
            }
        }
    }
}
