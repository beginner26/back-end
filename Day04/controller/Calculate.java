package Day04.controller;

public class Calculate {

    public static void hitungVolume(int p, int l, int t) {
        System.out.println("Volume Balok = " + (p * l * t));
    }

    public static void hitungVolume(double pi, int r) {
        System.out.println("Volume Bola = " + ((4.0 / 3) * pi * r * r * r));
    }

    public static void hitungVolume(double pi, int r, int t) {
        System.out.println("Volume Tabung = " + ((pi * r * r) * t));
    }


}
