package Day04.model;

public class Mahasiswa {
    String nama, jurusan;
    int nilai_fisika, nilai_kimia, nilai_biologi;


    public Mahasiswa() {
        this.nama = "Default";
        this.jurusan = "Default";
        this.nilai_biologi = 0;
        this.nilai_fisika = 0;
        this.nilai_kimia = 0;

        System.out.println(this.nama + " " + this.jurusan);
        System.out.println(this.nilai_fisika + " " + this.nilai_biologi + " " + this.nilai_kimia);
    }

    public Mahasiswa(String nama, String jurusan) {
        this.nama = nama;
        this.jurusan = jurusan;
        this.nilai_biologi = 0;
        this.nilai_fisika = 0;
        this.nilai_kimia = 0;

        System.out.println(this.nama + " " + this.jurusan);
        System.out.println(this.nilai_fisika + " " + this.nilai_biologi + " " + this.nilai_kimia);
    }

    public Mahasiswa(String nama, String jurusan, int fisika, int biologi, int kimia) {
        this.nama = nama;
        this.jurusan = jurusan;
        this.nilai_biologi = biologi;
        this.nilai_fisika = fisika;
        this.nilai_kimia = kimia;

        System.out.println(this.nama + " " + this.jurusan);
        System.out.println(this.nilai_fisika + " " + this.nilai_biologi + " " + this.nilai_kimia);
    }


}
