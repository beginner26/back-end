package Day04.model;

public class Mahasiswa_tugas5 {
    String nama;
    int nilai_fisika, nilai_kimia, nilai_biologi;


    public Mahasiswa_tugas5(String nama, int fisika, int biologi, int kimia) {
        this.nama = nama;
        this.nilai_biologi = biologi;
        this.nilai_fisika = fisika;
        this.nilai_kimia = kimia;
    }

    public String getData() {
        return this.nama + "," + this.nilai_biologi + "," + this.nilai_fisika + "," + this.nilai_kimia;
    }
}
