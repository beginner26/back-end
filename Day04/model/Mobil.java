package Day04.model;

public class Mobil {
    public String merek;
    public String warna;

    public void info() {
        System.out.println("Merek Mobil : " + this.merek);
        System.out.println("Warna Mobil : " + this.warna);
    }

    public void berjalan() {
        System.out.println(this.merek + " warna " + this.warna + " sedang berjalan");
    }

    public void berhenti() {
        System.out.println(this.merek + " warna " + this.warna + " sedang berhenti");
    }
}
