package Day04;
/* Assignment 3
 *  Latihan menggunakan Method Overloading
 */

import Day04.controller.Calculate;

import java.util.Scanner;

public class Assignment3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Calculate calc = new Calculate();

        int userPilih = 0;

        while (userPilih != 4) {
            System.out.println("Menu");
            System.out.println("1. Volume Balok");
            System.out.println("2. Volume Bola");
            System.out.println("3. Volume Tabung");
            System.out.println("4. EXIT");

            System.out.print("Pilih Menu = ");
            userPilih = input.nextInt();

            int panjang, lebar, tinggi, r;
            double pi;

            switch (userPilih) {
                case 1:
                    System.out.println("Menghitung Volume Balok");
                    System.out.print("Panjang = ");
                    panjang = input.nextInt();
                    System.out.print("Lebar = ");
                    lebar = input.nextInt();
                    System.out.print("Tinggi = ");
                    tinggi = input.nextInt();
                    calc.hitungVolume(panjang, lebar, tinggi);
                    break;
                case 2:
                    System.out.println("Menghitung Volume Bola");
                    System.out.print("Pi = ");
                    pi = input.nextDouble();
                    System.out.print("Jari - jari = ");
                    r = input.nextInt();
                    calc.hitungVolume(pi, r);
                    break;
                case 3:
                    System.out.println("Menghitung Volume Tabung");
                    System.out.print("Pi = ");
                    pi = input.nextDouble();
                    System.out.print("Jari - jari = ");
                    r = input.nextInt();
                    System.out.print("Tinggi = ");
                    tinggi = input.nextInt();
                    calc.hitungVolume(pi, r, tinggi);
                    break;

                case 4:
                    System.out.println("Keluar Program");
                    break;

                default:
                    System.out.println("Masukan pilihan yang sesuai");
                    break;
            }
        }
    }
}
