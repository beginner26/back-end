package Day04;
/* Assignment 1
 *  Latihan menggunakan ArrayList dan Membuat Object
 */

import Day04.model.Mobil;

import java.util.Scanner;

public class Assignment1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String userInputMerek;
        String userInputWarna;
        System.out.print("Masukan Merek Mobil: ");
        userInputMerek = input.nextLine();
        System.out.print("Masukan Warna Mobil: ");
        userInputWarna = input.nextLine();

        Mobil mobil1 = new Mobil();
        mobil1.merek = userInputMerek;
        mobil1.warna = userInputWarna;

        mobil1.info();
        mobil1.berjalan();
        mobil1.berhenti();

        System.out.print("Masukan Merek Mobil: ");
        userInputMerek = input.nextLine();
        System.out.print("Masukan Warna Mobil: ");
        userInputWarna = input.nextLine();

        Mobil mobil2 = new Mobil();
        mobil2.merek = userInputMerek;
        mobil2.warna = userInputWarna;

        mobil2.info();
        mobil2.berjalan();
        mobil2.berhenti();

        input.close();
    }
}
