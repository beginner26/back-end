package Day05;

/* EXAM 01
 *  Ujian pertama
 */

import Day05.model.Staff;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Exam1 {
    public static void main(String[] args) {
        Scanner inputI = new Scanner(System.in);
        Scanner inputS = new Scanner(System.in);
        ArrayList<Staff> arrStaff = new ArrayList<>();

        Staff newStaff = new Staff(0, "", 0);

        int userInput = 0;
        int target = -1;

        while (userInput != 99) {
            System.out.println("Menu");
            System.out.println("1. Buat Staff");
            System.out.println("2. Edit atau Delete Staff");
            System.out.println("3. Tampilkan Data Semua Staff");
            System.out.println("4. Absensi Staff");
            System.out.println("5. Hitung Tunjangan");
            System.out.println("6. Hitung Total Gaji");
            System.out.println("7. Laporan Gaji (Sort by ID)");
            System.out.println("99. Exit");

            System.out.print("Pilih menu: ");
            userInput = inputI.nextInt();

            switch (userInput) {
                case 1:
                    // Cek id sebelum dimasukan ke array list
                    boolean bolehInput = true;

                    // 1. Masukan ID
                    System.out.print("Input ID: ");
                    int id = inputI.nextInt();
                    // 2. Cek apakah ID sudah ada
                    for (Staff dataStaff : arrStaff) {
                        if (dataStaff.id == id) {
                            bolehInput = false;
                        }
                    }
                    // 3. Jika ID sudah ada maka masukan ulang ID
                    while (!bolehInput) {
                        System.out.println("ID sudah ada, masukan ID yang lain");
                        System.out.print("Input ID: ");
                        id = inputI.nextInt();
                        bolehInput = true;
                        // 2. Cek lagi apakah ID sudah ada
                        for (Staff dataStaff : arrStaff) {
                            if (dataStaff.id == id) {
                                bolehInput = false;
                            }
                        }
                    }

                    // 4. Jika ID belum ada maka buat object baru
                    System.out.print("Input Nama: ");
                    String nama = inputS.nextLine();
                    System.out.print("Input Gapok: ");
                    int gajiPokok = inputI.nextInt();
                    newStaff = new Staff(id, nama, gajiPokok);
                    arrStaff.add(newStaff);
                    break;

                case 2:
                    System.out.println("Pilih dari 2 pilihan dibawah");
                    System.out.println("a. Edit Staff (yang dapat di edit adalah selain ID dan Absensi)");
                    System.out.println("b. Delete Staff dari Array List");
                    System.out.print("Masukan pilihan: ");
                    String userInputString = inputS.nextLine();

                    if (userInputString.equals("a")) {
                        System.out.println("Anda memilih menu a");
                        System.out.print("Masukan ID yang ingin diubah: ");
                        userInput = inputI.nextInt();

                        for (Staff dataStaff : arrStaff) {
                            if (dataStaff.id == userInput) {
                                System.out.print("Masukan Nama: ");
                                dataStaff.nama = inputS.nextLine();
                                System.out.print("Masukan Gaji Pokok: ");
                                dataStaff.gajiPokok = inputI.nextInt();
                            }
                        }

                    } else if (userInputString.equals("b")) {
                        System.out.println("Anda memilih menu b");
                        System.out.print("Masukan ID Staff yang ingin dihapus: ");
                        userInput = inputI.nextInt();

                        // Mencari object yang mempunyai id yang user inputkan
                        for (Staff dataStaff : arrStaff) {
                            if (dataStaff.id == userInput) {
                                target = arrStaff.indexOf(dataStaff);
                            }
                        }

                        // Menghapus data staff
                        if (target != -1) {
                            System.out.println("Berhasil menghapus data staff");
                            arrStaff.remove(target);
                            target = -1;
                        } else {
                            System.out.println("Gagal menghapus karena ID salah");
                        }

                    } else {
                        System.out.println("Masukan sesuai menu");
                    }
                    break;

                case 3:
                    // Menampilkan semua data staff pada arraylist arrStaff
                    for (Staff dataStaff : arrStaff) {
                        System.out.println("ID: " + dataStaff.id);
                        System.out.println("Nama: " + dataStaff.nama);
                        System.out.println("Gaji Pokok: " + dataStaff.gajiPokok);
                    }
                    break;

                case 4:
                    // Melakukan absensi staff
                    System.out.print("Masukan ID staff untuk absen: ");
                    id = inputI.nextInt();
                    for (Staff dataStaff : arrStaff) {
                        if (dataStaff.id == id) {
                            dataStaff.absensiMethod();
                        }
                    }
                    break;

                case 5:
                    // Memanggil method yang ada pada semua object
                    for (Staff dataStaff : arrStaff) {
                        dataStaff.hitungTunjanganMakan();
                        dataStaff.hitungTunjanganTransport();
                    }
                    System.out.println("Berhasil menghitung tunjangan");
                    break;

                case 6:
                    // Memanggil method yang ada pada semua object
                    for (Staff dataStaff : arrStaff) {
                        dataStaff.hitungTotalGaji();
                    }
                    System.out.println("Berhasil menghitung total gaji");
                    break;

                case 7:
                    // Sorting arraylist berdasarkan ID pada object
                    Collections.sort(arrStaff, new Comparator<Staff>() {
                        @Override
                        public int compare(Staff o1, Staff o2) {
                            return o1.id - o2.id;
                        }
                    });

                    // Mengubah angka ke terbilang
                    for (Staff dataStaff : arrStaff) {
                        dataStaff.terbilang = dataStaff.angkaToTerbilang((long) dataStaff.totalGaji);
                    }

                    // Menampilkan laporan sesuai dengan format
                    System.out.println("Laporan Gaji");
                    System.out.println("ID\tNama\tAbsensi\tTotal Gaji\t\t Terbilang");
                    for (Staff dataStaff : arrStaff) {
                        System.out.printf("%s\t%s\t%s\t\t%s\t\t\t%s\n", dataStaff.id, dataStaff.nama, dataStaff.absensi, dataStaff.totalGaji, dataStaff.terbilang);
                    }

                    break;

                case 99:
                    System.out.println("Keluar Dari Program");
                    break;

                default:
                    System.out.println("Pilih sesuai menu");
                    break;
            }
        }
    }
}
