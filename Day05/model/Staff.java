package Day05.model;

public class Staff {
    public int id = 0;
    public String nama = "";
    public int tunjanganMakan = 0;
    public int tujanganTransport = 0;
    public int gajiPokok = 0;
    public int absensi = 20;
    public int totalGaji = 0;
    public String terbilang = "";

    public Staff(int id, String nama, int gajiPokok) {
        this.id = id;
        this.nama = nama;
        this.gajiPokok = gajiPokok;
    }

    public void absensiMethod() {
        if (this.absensi < 22) {
            this.absensi++;
            System.out.println("Absen berhasil, jumlah absen " + this.absensi);
        } else {
            System.out.println("Absen gagal, karena sudah melebihi batas yang ditentukan");
        }
    }

    public void hitungTunjanganMakan() {
        this.tunjanganMakan = this.absensi * 20000;
    }

    public void hitungTunjanganTransport() {
        this.tujanganTransport = this.absensi * 50000;
    }

    public void hitungTotalGaji() {
        this.totalGaji = this.gajiPokok + this.tujanganTransport + this.tunjanganMakan;
    }

    public static String angkaToTerbilang(Long angka) {
        String[] huruf = {"", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"};
        if (angka < 12)
            return huruf[angka.intValue()];
        if (angka >= 12 && angka <= 19)
            return huruf[angka.intValue() % 10] + " Belas";
        if (angka >= 20 && angka <= 99)
            return angkaToTerbilang(angka / 10) + " Puluh " + huruf[angka.intValue() % 10];
        if (angka >= 100 && angka <= 199)
            return "Seratus " + angkaToTerbilang(angka % 100);
        if (angka >= 200 && angka <= 999)
            return angkaToTerbilang(angka / 100) + " Ratus " + angkaToTerbilang(angka % 100);
        if (angka >= 1000 && angka <= 1999)
            return "Seribu " + angkaToTerbilang(angka % 1000);
        if (angka >= 2000 && angka <= 999999)
            return angkaToTerbilang(angka / 1000) + " Ribu " + angkaToTerbilang(angka % 1000);
        if (angka >= 1000000 && angka <= 999999999)
            return angkaToTerbilang(angka / 1000000) + " Juta " + angkaToTerbilang(angka % 1000000);
        if (angka >= 1000000000 && angka <= 999999999999L)
            return angkaToTerbilang(angka / 1000000000) + " Milyar " + angkaToTerbilang(angka % 1000000000);
        if (angka >= 1000000000000L && angka <= 999999999999999L)
            return angkaToTerbilang(angka / 1000000000000L) + " Triliun " + angkaToTerbilang(angka % 1000000000000L);
        if (angka >= 1000000000000000L && angka <= 999999999999999999L)
            return angkaToTerbilang(angka / 1000000000000000L) + " Quadrilyun " + angkaToTerbilang(angka % 1000000000000000L);
        return "";
    }
}
