package Day08;

/* Assignment 3
 *  Latihan multithreading untuk mencetak laporan ke console dan menulis file
 */

import java.io.FileWriter;
import java.util.*;

class MultiThread1 extends Thread {
    private Map allMhs;

    public MultiThread1(Map allMhs) {
        this.allMhs = allMhs;
    }

    public void run() {
//        for (int i = 0; i < 5; i++) {
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("File");
//        }
        // Membuat file dan mengisi data berdasarkan Map-HashMap
        try {
            FileWriter fw = new FileWriter("C:\\tmp\\assignment3.txt");
            TreeMap<Integer, Mahasiswa> tm = new TreeMap<Integer, Mahasiswa>(this.allMhs);
            Iterator itr = tm.keySet().iterator();
            while (itr.hasNext()) {
                //Converting to Map.Entry so that we can get key and value separately
                int key = (int) itr.next();
                Mahasiswa mhs = (Mahasiswa) this.allMhs.get(key);

                fw.write("ID: " + mhs.getId() + "\nNama: " + mhs.getNama() + "\nB. Inggris: " + mhs.getNilai().get(0) + "\nFisika: " + mhs.getNilai().get(1) + "\nAlgoritma: " + mhs.getNilai().get(2) + "\n");
            }
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Success...");
    }
}

class MultiThread2 extends Thread {
    private Map allMhs;

    public MultiThread2(Map allMhs) {
        this.allMhs = allMhs;
    }

    public void run() {
//        for (int i = 0; i < 5; i++) {
//            try {
//                Thread.sleep(500);
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//            System.out.println("Console");
//        }

        TreeMap<Integer, Mahasiswa> tm = new TreeMap<Integer, Mahasiswa>(this.allMhs);
        Iterator itr = tm.keySet().iterator();
        System.out.println("ID\tNama\tBhs Inggris\tFisika\tAlgoritma");
        while (itr.hasNext()) {
            //Converting to Map.Entry so that we can get key and value separately
            int key = (int) itr.next();
            Mahasiswa mhs = (Mahasiswa) this.allMhs.get(key);
            System.out.println(mhs.getId() + "\t" + mhs.getNama() + "\t\t" + mhs.getNilai().get(0) + "\t\t" + mhs.getNilai().get(1) + "\t\t" + mhs.getNilai().get(2));
        }
    }
}

public class Assignment3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Map allMhs = new HashMap<>();

        int userInput = 0;
        TreeMap<Integer, Mahasiswa> tm;
        Iterator itr;

        while (userInput != 4) {
            System.out.println("Menu");
            System.out.println("1. Create & Input Data Mahasiswa");
            System.out.println("2. Tampilkan laporan nilai data mahasiswa");
            System.out.println("3. Tampilkan di layar & tulis ke file (Multithreading)");
            System.out.println("4. Exit");
            System.out.print("Pilih Menu: ");
            userInput = input.nextInt();
            switch (userInput) {
                case 1:
                    System.out.print("Masukan ID: ");
                    int id = input.nextInt();
                    System.out.print("Masukan Nama: ");
                    String nama = input.next();

                    // Mengisi ArrayList berdasarkan user input
                    ArrayList nilaiInput = new ArrayList();
                    for (int i = 0; i < 3; i++) {
                        System.out.print("Masukan Nilai " + (i + 1) + ": ");
                        Double inputNilai = input.nextDouble();
                        nilaiInput.add(inputNilai);
                    }
                    // Menambahkan Object ke Map-HashMap
                    allMhs.put(id, new Mahasiswa(id, nama, nilaiInput));
                    System.out.println("Berhasil menambahkan data mahasiswa");
                    break;

                case 2:
                    tm = new TreeMap<Integer, Mahasiswa>(allMhs);
                    itr = tm.keySet().iterator();
                    // Menampilkan Laporan ke Console
                    System.out.println("ID\tNama\tBhs Inggris\tFisika\tAlgoritma");
                    while (itr.hasNext()) {
                        //Converting to Map.Entry so that we can get key and value separately
                        int key = (int) itr.next();
                        Mahasiswa mhs = (Mahasiswa) allMhs.get(key);
                        System.out.println(mhs.getId() + "\t" + mhs.getNama() + "\t\t" + mhs.getNilai().get(0) + "\t\t" + mhs.getNilai().get(1) + "\t\t" + mhs.getNilai().get(2));
                    }
                    break;

                case 3:
                    // Membuat file dan mengisi data berdasarkan Map-HashMap
                    MultiThread1 t1 = new MultiThread1(allMhs);
                    t1.start();

                    // Menampilkan data semua mahasiswa ke console
                    MultiThread2 t2 = new MultiThread2(allMhs);
                    t2.start();
                    break;

                case 4:
                    System.out.println("Keluar dari Program");
                    break;
            }
        }
    }
}
