package Day08;

interface Worker {
    abstract void tambahAbsensi();
}

public class Staff implements Worker {
    int id;
    String nama;
    int absensi = 20;


    int gapok;
    int tunjanganMakan;
    int tunjanganTransport;

    int totalGaji;

    Staff(int id, String nama, int gapok) {
        this.id = id;
        this.nama = nama;
        this.gapok = gapok;
    }

    public int getGapok() {
        return gapok;
    }

    public void setGapok(int gapok) {
        this.gapok = gapok;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAbsensi() {
        return absensi;
    }

    public int getTotalGaji() {
        return totalGaji;
    }

    public void setTotalGaji(int totalGaji) {
        this.totalGaji = totalGaji;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

    public void tambahAbsensi() {
        this.absensi++;
    }

    public void hitungTunjangan() {
        this.tunjanganMakan = this.absensi * 20000;
        this.tunjanganTransport = this.absensi * 50000;
    }

    public void hitungTotalGaji() {
        this.totalGaji = this.gapok + this.tunjanganMakan + this.tunjanganTransport;
    }
}