package Day08;

import java.util.*;

public class Main {
    Map m = new HashMap();
    Scanner input = new Scanner(System.in);

    void inputDummy() {
        m.put(3, new Staff(3, "Shawn", 500));
        m.put(1, new Staff(1, "Tomy", 600));
        m.put(2, new Staff(2, "John", 700));
    }

    public void addStaff() {

        System.out.print("Input ID : ");
        int id = this.input.nextInt();
        System.out.print("Input Nama : ");
        String nama = this.input.next();
        System.out.print("Input Gaji Pokok : ");
        int gapok = this.input.nextInt();

        Staff st = new Staff(id, nama, gapok);

        m.put(id, st);

    }

    public void tambahAbsensi() {

        System.out.print("Masukin ID: ");
        int id = this.input.nextInt();

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if (st.getId() == id) {
                st.tambahAbsensi();
                System.out.println("Berhasil menambahkan absensi");
            }
        }
    }

    public void hitungTunjangan() {

        System.out.print("Masukin ID: ");
        int id = this.input.nextInt();

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();

            if (st.getId() == id) {
                st.hitungTunjangan();
                System.out.println("Berhasil menghitung tunjangan");
            }
        }
    }

    public void hitungTotalGaji() {

        Set set = m.entrySet();//Converting to Set so that we can traverse
        Iterator itr = set.iterator();
        while (itr.hasNext()) {

            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry) itr.next();
            //Convert to Staff
            Staff st = (Staff) entry.getValue();


            st.hitungTotalGaji();

        }
        System.out.println("Berhasil menghitung total gaji");
    }

    public void viewStaff() {
        TreeMap<Integer, Staff> tm = new TreeMap<Integer, Staff>(m);
        Iterator itr = tm.keySet().iterator();
        System.out.println("ID\tNama\tAbsensi/hari\tTotal Gaji");
        while (itr.hasNext()) {
            int key = (int) itr.next();
            Staff st = (Staff) m.get(key);
            System.out.printf("%s\t%s\t\t%s\t\t\t%s\n", st.getId(), st.getNama(), st.getAbsensi(), st.getTotalGaji());
        }
    }

    public static void main(String args[]) {

        Main obj = new Main();

        int pil = 0;

        do {
            System.out.println("MENU");
            System.out.println("0. Buat Dummy Data");
            System.out.println("1. Buat Staff");
            System.out.println("2. Tambah Absensi");
            System.out.println("3. Hitung Tunjangan");
            System.out.println("4. Hitung Total Gaji");
            System.out.println("5. Laporan Gaji");


            System.out.println("6. EXIT");

            System.out.print("Input nomor : ");
            pil = obj.input.nextInt();

            switch (pil) {
                // performs addition between numbers
                case 1:
                    obj.addStaff();
                    break;
                case 2:
                    obj.tambahAbsensi();
                    break;
                case 3:
                    obj.hitungTunjangan();
                    break;
                case 4:
                    obj.hitungTotalGaji();
                    break;
                case 5:
                    obj.viewStaff();
                    break;

                case 0:
                    obj.inputDummy();
                    break;
            }
            System.out.println();

        } while (pil != 6);

        // closing the scanner object
        obj.input.close();

    }


}