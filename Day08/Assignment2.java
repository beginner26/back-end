package Day08;

/* Assignment 2
 *  Latihan menggunakan Thread untuk multithreading
 */

class Multi1 extends Thread {
    public void run() {
        for (int i = 1; i <= 10; i++) {
            try {
                // Menjeda thread agar mengetahui apakah multithreading atau tidak
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(i);
        }
    }
}

class Multi2 extends Thread {
    public void run() {
        for (int i = 0; i <= 10; i++) {
            try {
                // Menjeda thread agar mengetahui apakah multithreading atau tidak
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("G2Academy");
        }
    }
}

public class Assignment2 {
    public static void main(String[] args) {
        Thread t1 = new Multi1();
        t1.start();
        Thread t2 = new Multi2();
        t2.start();
    }
}
