package Day08;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

public class Homework {
    public boolean validateEmail(String email) {
        // Email must follow this format [huruf&angka]@[provide].[domain]
        return Pattern.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$", email);
    }

    public boolean validatePassword(String password) {
        // Password must contain 8 characters, at least one uppercase, one lowercase, one spescial character and one number
        return Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", password);
    }

    public static void main(String[] args) {
        Homework obj = new Homework();
        Scanner input = new Scanner(System.in);

        Map allMhs = new HashMap<>();

        String usernameFile = "";
        String passwordFile = "";

        // Membaca file dan memasukannya ke variabel usernameFile
        try {
            FileReader fr = new FileReader("C:\\tmp\\username.txt");
            BufferedReader br = new BufferedReader(fr);
            int i;
            while ((i = br.read()) != -1) {
                usernameFile = usernameFile + (char) i;
            }
            System.out.println("Success......");
            br.close();
            fr.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        // Membaca file dan memasukannya ke variabel password
        try {
            FileReader fr = new FileReader("C:\\tmp\\password.txt");
            BufferedReader br = new BufferedReader(fr);
            int i;
            while ((i = br.read()) != -1) {
                passwordFile = passwordFile + (char) i;
            }
            System.out.println("Success......");
            br.close();
            fr.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("Login");
        System.out.print("Username: ");
        String username = input.next();
        System.out.print("Password: ");
        String password = input.next();


        if (obj.validateEmail(username) && obj.validatePassword(password) && username.equals(usernameFile) && password.equals(passwordFile)) {
//        if (true) {
            try {
                for (int i = 0; i < 10; i++) {
                    System.out.print(".");
                    sleep(300);
                }
                System.out.println("\nBerhasil Login");
                sleep(500);

                System.out.println("");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            int userInput = 0;
            int id;
            String nama;

            while (userInput != 4) {
                System.out.println("----- Menu -----");
                System.out.println("1. Buat Mahasiswa");
                System.out.println("2. Edit / Hapus Data Mahasiswa");
                System.out.println("3. Laporan Mahasiswa");
                System.out.println("4. Exit");
                System.out.print("Pilih Menu: ");
                userInput = input.nextInt();

                switch (userInput) {
                    case 1:
                        System.out.print("Masukan ID: ");
                        id = input.nextInt();
                        System.out.print("Masukan Nama: ");
                        nama = input.next();

                        ArrayList nilaiInput = new ArrayList();
                        for (int i = 0; i < 3; i++) {
                            System.out.print("Masukan Nilai " + (i + 1) + ": ");
                            Double inputNilai = input.nextDouble();
                            nilaiInput.add(inputNilai);
                        }
                        allMhs.put(id, new Mahasiswa(id, nama, nilaiInput));
                        System.out.println("Berhasil membuat data Mahasiswa");
                        break;

                    case 2:
                        System.out.println("A. Edit Data Mahasiswa");
                        System.out.println("B. Delete Data Mahasiswa");
                        System.out.print("Pilih Menu: ");
                        String userInputS = input.next();
                        if (userInputS.equalsIgnoreCase("a")) {
                            System.out.println("Edit Data");
                            System.out.print("Masukan ID: ");
                            id = input.nextInt();
                            Set set = allMhs.entrySet();//Converting to Set so that we can traverse
                            Iterator itr = set.iterator();
                            while (itr.hasNext()) {
                                Map.Entry entry = (Map.Entry) itr.next();
                                Mahasiswa mhs = (Mahasiswa) entry.getValue();
                                if (mhs.getId() == id) {
                                    System.out.print("Masukan Nama: ");
                                    nama = input.next();
                                    mhs.setNama(nama);
                                    System.out.println("Berhasil Edit");
                                }
                            }
                        } else if (userInputS.equalsIgnoreCase("b")) {
                            System.out.println("Delete Data");
                            System.out.print("Masukan ID: ");
                            id = input.nextInt();
                            Set set = allMhs.entrySet();//Converting to Set so that we can traverse
                            Iterator itr = set.iterator();
                            while (itr.hasNext()) {
                                Map.Entry entry = (Map.Entry) itr.next();
                                Mahasiswa mhs = (Mahasiswa) entry.getValue();
                                if (mhs.getId() == id) {
                                    allMhs.remove(id);
                                    System.out.println("Berhasil Delete");
                                }
                            }
                        }
                        break;
                    case 3:
                        TreeMap<Integer, Mahasiswa> tm = new TreeMap<Integer, Mahasiswa>(allMhs);
                        Iterator itr = tm.keySet().iterator();
                        System.out.println("ID\tNama\tBhs Inggris\tFisika\tAlgoritma");
                        while (itr.hasNext()) {
                            //Converting to Map.Entry so that we can get key and value separately
                            int key = (int) itr.next();
                            Mahasiswa mhs = (Mahasiswa) allMhs.get(key);
                            System.out.println(mhs.getId() + "\t" + mhs.getNama() + "\t\t" + mhs.getNilai().get(0) + "\t\t" + mhs.getNilai().get(1) + "\t\t" + mhs.getNilai().get(2));
                        }
                        break;
                    case 4:
                        System.out.println("Keluar dari Program");
                        break;
                }
            }

        } else {
            // Jika username dan password salah
            try {
                for (int i = 0; i < 10; i++) {
                    System.out.print(".");
                    sleep(300);
                }
                System.out.println("\nGagal Login");

                System.out.println("");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
