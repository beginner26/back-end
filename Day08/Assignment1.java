package Day08;
/* Assignment 1
 *  Latihan menggunakan Regex untuk validasi String
 */

import java.util.Scanner;
import java.util.regex.Pattern;

public class Assignment1 {
    public boolean validateEmail(String email) {
        // Email must follow this format [huruf&angka]@[provide].[domain]
        return Pattern.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$", email);
    }

    public boolean validatePassword(String password) {
        // Password must contain 8 characters, at least one uppercase, one lowercase, one spescial character and one number
        return Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$", password);
    }

    public static void main(String[] args) {
        Assignment1 obj = new Assignment1();
        Scanner input = new Scanner(System.in);

        System.out.println("Login");
        System.out.print("Username: ");
        String username = input.nextLine();
        System.out.print("Password: ");
        String password = input.nextLine();

        // Melakukan validasi username dan password
        if (obj.validateEmail(username) && obj.validatePassword(password)) {
            System.out.println("Berhasil Login");
        } else {
            System.out.println("Regex Salah");
        }
    }
}
