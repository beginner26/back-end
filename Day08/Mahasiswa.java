package Day08;

import java.util.ArrayList;

public class Mahasiswa {
    int id;
    String nama;
    ArrayList<Double> nilai = new ArrayList();

    public Mahasiswa(int id, String nama, ArrayList<Double> nilai) {
        this.id = id;
        this.nama = nama;
        this.nilai = nilai;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public ArrayList getNilai() {
        return nilai;
    }

    public void setNilai(ArrayList nilai) {
        this.nilai = nilai;
    }


}
