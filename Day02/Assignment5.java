package Day02;
/* Assignment 5
 *  Latihan menggunakan do while / while
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int userInput;

        // Melakukan pengulangan selama input tidak sama dengan 4
        do {
            System.out.println("Menu");
            System.out.println("1. Cetak Menu 1");
            System.out.println("2. Cetak Menu 2");
            System.out.println("3. Cetak Menu 3");
            System.out.println("4. Exit");
            System.out.print("Input nomor: ");
            userInput = input.nextInt();

            // Masuk menu sesuai dengan inputan user
            switch (userInput) {
                case 1:
                    System.out.println("Menu 1 \n");
                    break;
                case 2:
                    System.out.println("Menu 2 \n");
                    break;
                case 3:
                    System.out.println("Menu 3 \n");
                    break;
                case 4:
                    System.out.println("---Keluar Dari Program---");
                    break;
                default:
                    System.out.println("Harap memilih menu yang ada \n");
                    break;
            }
        } while (userInput != 4);

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}