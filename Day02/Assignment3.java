package Day02;
/* Assignment 3
 *  Mendapatkan input dari user menggunakan Scanner, dan menampilkan zodiak berdasarkan tanggal lahir
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Menerima input dari user
        System.out.print("Nama = ");
        String nama = input.nextLine();

        System.out.print("Tanggal Lahir = ");
        String tanggal = input.nextLine();

        System.out.print("Bulan Lahir = ");
        String bulan = input.nextLine();

        System.out.print("Tahun Lahir = ");
        String tahun = input.nextLine();

        int iTanggal = Integer.parseInt(tanggal);
        int iBulan = Integer.parseInt(bulan);

        int umur = 2022 - Integer.parseInt(tahun);

        String zodiak = "";

        // Menentukan zodiak berdasarkan tanggal lahir
        if ((iTanggal >= 1 && iTanggal <= 31) && (iBulan >= 1 && iBulan <= 12)) {
            if ((iTanggal >= 19 && iBulan == 4) || (iTanggal <= 13 && iBulan == 5)) {
                zodiak = "Aries";
            } else if ((iTanggal >= 14 && iBulan == 5) || (iTanggal <= 19 && iBulan == 6)) {
                zodiak = "Taurus";
            } else if ((iTanggal >= 20 && iBulan == 6) || (iTanggal <= 20 && iBulan == 7)) {
                zodiak = "Gemini";
            } else if ((iTanggal >= 21 && iBulan == 7) || (iTanggal <= 9 && iBulan == 8)) {
                zodiak = "Kanser";
            } else if ((iTanggal >= 10 && iBulan == 8) || (iTanggal <= 15 && iBulan == 9)) {
                zodiak = "Leo";
            } else if ((iTanggal >= 16 && iBulan == 9) || (iTanggal <= 30 && iBulan == 10)) {
                zodiak = "Virgo";
            } else if ((iTanggal >= 31 && iBulan == 10) || (iTanggal <= 22 && iBulan == 11)) {
                zodiak = "Libra";
            } else if ((iTanggal >= 23 && iBulan == 11) || (iTanggal <= 29 && iBulan == 11)) {
                zodiak = "Skorpio";
            } else if ((iTanggal >= 18 && iBulan == 12) || (iTanggal <= 18 && iBulan == 1)) {
                zodiak = "Sagitarius";
            } else if ((iTanggal >= 19 && iBulan == 1) || (iTanggal <= 15 && iBulan == 2)) {
                zodiak = "Kaprikornus";
            } else if ((iTanggal >= 16 && iBulan == 2) || (iTanggal <= 11 && iBulan == 3)) {
                zodiak = "Akuarius";
            } else if ((iTanggal >= 12 && iBulan == 3) || (iTanggal <= 18 && iBulan == 4)) {
                zodiak = "Pises";
            } else if ((iTanggal >= 30 && iBulan == 11) || (iTanggal <= 17 && iBulan == 12)) {
                zodiak = "Taurus";
            } else {
                zodiak = "Tidak ada";
            }
            System.out.println("Hai " + nama + ", zodiak kamu adalah " + zodiak + " dan umur kamu " + umur);
        } else {
            System.out.println("Format Salah");
        }

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}