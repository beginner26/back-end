package Day02;
/* Assignment 4
 *  Latihan menggunakan for loop
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Menerima input dari user
        System.out.print("Tahun Lahir = ");
        int tahunLahir = input.nextInt();

        System.out.print("Tahun Sekarang = ");
        int tahunSekarang = input.nextInt();

        int umur = tahunSekarang - tahunLahir;

        // Melakukan looping sesuai jumlah umur
        for (int i = umur; i > 0; i--) {
            System.out.println(umur + " tahun pada tahun " + tahunSekarang);
            umur--;
            tahunSekarang--;
        }

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}