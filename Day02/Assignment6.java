package Day02;
/* Assignment 6
 *  Latihan menggunakan do while / while
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int userInput;
        int panjang, lebar, tinggi, r, tahunLahir, tahunSekarang;
        double pi;

        // Melakukan pengulangan selama input tidak sama dengan 4
        do {
            System.out.println("Menu");
            System.out.println("1. Volume Balok");
            System.out.println("2. Volume Bola");
            System.out.println("3. Hitung Umur");
            System.out.println("4. Exit");
            System.out.print("Input nomor: ");
            userInput = input.nextInt();

            // Masuk menu sesuai dengan inputan user
            switch (userInput) {
                case 1:
                    System.out.print("Panjang = ");
                    panjang = input.nextInt();
                    System.out.print("Lebar = ");
                    lebar = input.nextInt();
                    System.out.print("Tinggi = ");
                    tinggi = input.nextInt();
                    System.out.println("Volume Balok = " + (panjang * lebar * tinggi));
                    break;

                case 2:
                    System.out.print("Pi = ");
                    pi = input.nextDouble();
                    System.out.print("Jari-jari = ");
                    r = input.nextInt();
                    System.out.println("Volume Bola = " + ((4.0 / 3) * pi * r * r * r));
                    break;

                case 3:
                    System.out.print("Masukan tahun lahir = ");
                    tahunLahir = input.nextInt();
                    System.out.print("Masukan tahun sekarang = ");
                    tahunSekarang = input.nextInt();
                    System.out.println("Umur anda = " + (tahunSekarang - tahunLahir));
                    break;

                case 4:
                    System.out.println("---Keluar Dari Program---");
                    break;

                default:
                    System.out.println("Harap memilih menu yang ada \n");
                    break;
            }
        } while (userInput != 4);

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}