package Day02;
/* Assignment 2
 *  Mendapatkan input dari user menggunakan Scanner, dan melakukan perhitungan
 */

// berikut package yang di import untuk mengambil input dari user

import java.util.Scanner;

public class Assignment2 {
    public static void main(String[] args) {
        // Membuat instance input dari Scanner Object
        Scanner input = new Scanner(System.in);

        // Menerima input dari user
        System.out.println("Choose an operator: +, -, *, or /");
        String operator = input.nextLine();

        System.out.println("Enter First Number");
        String firstNumber = input.nextLine();

        System.out.println("Enter Second Number");
        String secondNumber = input.nextLine();

        double iFirstNumber = Integer.parseInt(firstNumber);
        double iSecondNumber = Integer.parseInt(secondNumber);
        double hasil;

        // Switch condition untuk memilih operator aritmatik
        switch (operator) {
            case "+":
                hasil = iFirstNumber + iSecondNumber;
                break;
            case "-":
                hasil = iFirstNumber - iSecondNumber;
                break;
            case "*":
                hasil = iFirstNumber * iSecondNumber;
                break;
            case "/":
                hasil = iFirstNumber / iSecondNumber;
                break;
            default:
                hasil = 0;
                System.out.println("Salah memasukan Day01.Operator");
        }

        // Menampilkan hasil perhitungan ke console
        System.out.printf("%.1f %s %.1f = %.1f", iFirstNumber, operator, iSecondNumber, hasil);

        // Menutup inputan agar tidak memenuhi memory
        input.close();
    }
}